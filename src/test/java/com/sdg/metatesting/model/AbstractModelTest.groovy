package com.sdg.metatesting.model

import org.apache.commons.lang3.RandomStringUtils
import spock.lang.Specification

import javax.persistence.Column
import javax.persistence.Transient
import java.lang.annotation.Annotation
import java.lang.reflect.Field
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.time.LocalDate
import java.util.concurrent.ThreadLocalRandom

/**
 * Created by paulwiedel on 8/6/17.
 */
abstract class AbstractModelTest extends Specification {

    public static List<String> EXCLUDED_FIELDS = ["metaClass", "class"]
    final static Date dateValue = new Date(11235813L)
    final static Date otherDateValue = new Date(21345589L)
    public


    static Map defaultValues = ["int"             : 0,
                                "java.lang.String": "first",
                                "java.util.Date"  : dateValue,
                                "boolean"         : true,
                                "java.util.List"  : []
    ]
    public static Map defaultDiffValues = ["int"             : 1,
                                           "java.lang.String": "second",
                                           "java.util.Date"  : otherDateValue,
                                           "boolean"         : false,
                                           "java.util.List"  : ["value"],
    ]

    Map gettersByClass = new HashMap<Class, Map<String, Object>>()
    Map settersByClass = new HashMap<Class, Map<String, Object>>()
    Map typesByClass = new HashMap<Class, Map<String, Class>>()

    /**
     * Exercises the .equals and .hashCode methods for a JPA model object.
     * It is assumed that all fields that are not annotated with a @Transient annotation
     * define the identity of a model object.
     *
     * The approach of this test is to instantiate a two instances of the class, clazz: object and otherObject.
     * The test interrogates the fields from the instance and asserts that for each field there is a
     * getter and a setter method.
     *
     * For each setter, the test will invoke the method with a value of the method signature's type. Prior to
     * invoking the setter on object or otherObject, the test asserts that each object's .hashCode and .equals
     * method successfully indicates that each object is equal. It then invokes the next available setter on one of the
     * objects: object. After the setter is invoked on object, the test again asserts that .equals and .hashCode both
     * reflect that each object has a different state from the other. The test then invokes the same setter method on
     * otherObject with the same value. The test will then assert that each object's .equals and .hashCode reflect that
     * they both have the same state.
     *
     * This test indirectly asserts that the model object complies with the Java Bean pattern and the JPA standards for
     * .equals and .hashCode
     *
     * @param clazz A Java Class to test.
     */
    void testEqualsAndHashCode(Class clazz) {
        Object object = clazz.newInstance()
        // testing fundamental properties of .equals
        // no non-null object should be equal to null
        assert !object.equals(null)
        // no object should be equal to an instance of another class
        // assuming object is not an instance of Random
        assert !object.equals(Random.newInstance())

        Object otherObject = clazz.newInstance()
        // with an instance of the class, find all of the getters and setters
        interrogateObject(object)
        assert gettersByClass.get(clazz).keySet() == settersByClass.get(clazz).keySet()
        assert gettersByClass.get(clazz).keySet() == typesByClass.get(clazz).keySet()

        // based on getter return value
        for (String prop : gettersByClass.get(clazz).keySet()) {
            assertEqualsAndHashCode(object, otherObject, true)
            def setter = settersByClass.get(clazz).get(prop)
            Class argType = typesByClass.get(clazz).get(prop)
            try {
                Field field = clazz.getDeclaredField(prop)
                Annotation aTransient = field.getDeclaredAnnotation(Transient.class)
                if (!aTransient) {
                    setter.invoke(object, getValueForType(argType))
                    assertEqualsAndHashCode(object, otherObject, false)
                    setter.invoke(otherObject, getValueForType(argType))
                    assertEqualsAndHashCode(object, otherObject, true)
                    testEqualsAndHashCodeForSetter(setter, object, argType, otherObject)
                }
            } catch (NoSuchFieldException e) {
                assert false
            }
        }
    }

    /**
     * tests the toString method
     * @param clazz
     */
    void testToString(Class clazz) {
        Object object = clazz.newInstance()
        interrogateObject(object)
        assert gettersByClass.get(clazz).keySet() == settersByClass.get(clazz).keySet()
        assert gettersByClass.get(clazz).keySet() == typesByClass.get(clazz).keySet()
        for (String prop : gettersByClass.get(clazz).keySet()) {
            Class argType = typesByClass.get(clazz).get(prop)
            try {
                Field field = clazz.getField(prop)
                Annotation aTransient = field.getDeclaredAnnotation(Transient.class)
                if (!aTransient) {
                    Object prettyUniqueValue = getUniqueValueForType(argType)
                    assertStringDoesNotContain(object.toString(), prettyUniqueValue.toString())
                    def setter = settersByClass.get(clazz).get(prop)
                    setter.invoke(object, prettyUniqueValue)
                    assertStringContains(object.toString(), prettyUniqueValue.toString())
                }
            } catch (NoSuchFieldException e) {

            }
        }

    }

    void assertStringContains(String stringThatShouldContainOtherString, String stringThatShouldBeContainedWithinOtherString) {
        assert stringThatShouldContainOtherString.contains(stringThatShouldBeContainedWithinOtherString)
    }

    void assertStringDoesNotContain(String stringThatShouldNotContainOtherString, String stringThatShouldNotContainedWithOtherString) {
        assert !stringThatShouldNotContainOtherString.contains(stringThatShouldNotContainedWithOtherString)
    }

    /**
     * Creates an instance of a class and populates all of the attributes with unique-ish values.
     * @param clazz Class that will be populated.
     * @param ignoreForeignKeyConstaints if true, all foreign key constraints are set to null. This is to enable tests
     * that involve persisting data.
     * @return an instance with attributes populated
     */
    Object populateObjectWithAttributes(Class clazz, boolean ignoreForeignKeyConstaints) {
        Object object = clazz.newInstance()
        interrogateObject(object)
        for (String prop : gettersByClass.get(object.class).keySet()) {
            if ("id".equals(prop)) {
                continue
            }
            def setter = settersByClass.get(object.class).get(prop)
            Class argType = typesByClass.get(object.class).get(prop)
            Object prettyUniqueValue = null
            Field field = clazz.getDeclaredField(prop)
            Annotation columnAnnotation = field.getDeclaredAnnotation(Column.class)


            Type type = field.getGenericType()
            // using model package name to only
            if (argType.getName().indexOf("com.sdg.metatesting.model") >= 0) {
                prettyUniqueValue = populateObjectWithAttributes(argType, ignoreForeignKeyConstaints)
            } else if (Collection.class.isAssignableFrom(argType)) {
                List list = []
                // assuming all collections are Lists, will need to adjust if we start using sets
                for (Type t : ((ParameterizedType) type).getActualTypeArguments()) {
                    list << populateObjectWithAttributes(t, ignoreForeignKeyConstaints)
                }
                prettyUniqueValue = list

            } else {
                prettyUniqueValue = getUniqueValueForType(argType)
            }
            if (prettyUniqueValue) {
                setter.invoke(object, prettyUniqueValue)
            }
        }
        return object
    }

/**
 * Attempts to create a unique-ish value for a class
 * Intended to support testing toString
 * @param aClass
 * @return
 */
    Object getUniqueValueForType(Class aClass) {
        if (aClass.getName().equals(String.class.getName())) {
            return RandomStringUtils.random(ThreadLocalRandom.current().nextInt(1, 3), true, true) +
                    RandomStringUtils.random(ThreadLocalRandom.current().nextInt(1, 3), ((char[]) ['a', 'e', 'i', 'o', 'u', 'y'].toArray())) +
                    RandomStringUtils.random(ThreadLocalRandom.current().nextInt(2, 5), true, true)
        }
        if (aClass.getName().equals("int")) {
            return ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE)
        }
        if (aClass.getName().equals(Date.class.getName())) {
            Calendar calendar = Calendar.getInstance()
            calendar.setTime(new Date(ThreadLocalRandom.current().nextLong(0, new Date().getTime())))
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar.getTime()
        }
        if (aClass.getName().equals(List.class.getName())) {
            return [new Object()]
        }
        if (aClass.getName().equals(LocalDate.class.getName())) {
            return new LocalDate(
                    ThreadLocalRandom.current().nextInt(1900, 2100),
                    ThreadLocalRandom.current().nextInt(1, 12),
                    ThreadLocalRandom.current().nextInt(1, 28)
            )
        }
        if (aClass.getName().equals(Map.class.getName())) {
            return ["key": "value"]
        }
        getValueForType(aClass)
    }

    /**
     * Uses a setter method to exercise equals and hashCode behavior on two objects.
     * The object
     * @param setter setter method
     * @param object one of the objects
     * @param argType the class of the argument for the setter
     * @param otherObject the "other" object that object is being compared to
     */
    private void testEqualsAndHashCodeForSetter(setter, object, Class argType, otherObject) {
        setter.invoke(object, getValueForType(argType))
        setter.invoke(otherObject, getValueForType(argType))
        assertEqualsAndHashCode(object, otherObject, true)
        setter.invoke(otherObject, getValueForType(argType))
        assertEqualsAndHashCode(object, otherObject, false)
        setter.invoke(object, getValueForType(argType))
        assertEqualsAndHashCode(object, otherObject, true)
    }

    /**
     * gets method signature information from an object instance. Getter and setter maps will be used for testing.
     * @param object
     */
    void interrogateObject(Object object) {
        if (gettersByClass.containsKey(object.class) &&
                settersByClass.containsKey(object.class) &&
                typesByClass.containsKey(object.class)) {
            // getters, setters, and types have already been discovered
            return
        }
        Map getters = new HashMap<String, Object>()
        Map setters = new HashMap<String, Object>()
        Map types = new HashMap<String, Class>()
        object.properties.each { prop, val ->
            if (prop in EXCLUDED_FIELDS) {
                return
            }

            String getterName = "get" + prop.toString().substring(0, 1).toUpperCase() + prop.toString().substring(1)
            String setterName = "set" + prop.toString().substring(0, 1).toUpperCase() + prop.toString().substring(1)
            String booleanGetterName = "is" + prop.toString().substring(0, 1).toUpperCase() + prop.toString().substring(1)

            def getter = getMethodByName(object, getterName)
            // taking boolean into account
            if (getter == null) {
                getter = getMethodByName(object, booleanGetterName)
            }
            def setter = getMethodByName(object, setterName)

            Class returnType = getter.cachedMethod.returnType

            getters.put(prop.toString(), getter)
            setters.put(prop.toString(), setter)
            types.put(prop.toString(), returnType)
        }
        gettersByClass.put(object.class, getters)
        settersByClass.put(object.class, setters)
        typesByClass.put(object.class, types)
    }

    /**
     * convenience method for getting a method by name
     * @param pojo
     * @param methodName
     * @return
     */
    def getMethodByName(Object pojo, String methodName) {
        def value = null
        pojo.metaClass.methods.each { method ->
            if (method.name.equals(methodName)) {
                value = method
                return
            }
        }
        return value
    }

    /**
     * asserts that an object is equal to itself and its hashcode method's result equals itself
     * @param object
     */
    void assertHashCodeAndEqualsOnSelf(Object object) {
        object.hashCode() == object.hashCode()
        object.equals(object)
    }

    /**
     * perform assertions of expected equals and hashCode between two objects
     * @param object an object to be compared
     * @param objectOther another object to be compared
     * @param expectEquals the expectation of whether the equals and hashcode should match between object and objectOther
     */
    void assertEqualsAndHashCode(Object object, Object objectOther, boolean expectEquals) {
        // every object should be equal to itself
        assertHashCodeAndEqualsOnSelf(object)
        assertHashCodeAndEqualsOnSelf(objectOther)

        // if expectedEquals, test for equality and matching hashcodes
        if (expectEquals) {
            object.hashCode() == objectOther.hashCode()
            object.equals(objectOther)
            objectOther.equals(object)
        } else {
            // if !expectEquals, test for not equal and non-matching hashcodes
            object.hashCode() != objectOther.hashCode()
            !object.equals(objectOther)
            !objectOther.equals(object)
        }
    }
}

