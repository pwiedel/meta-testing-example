package com.sdg.metatesting.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.Transient;


/**
 * Created by paulwiedel on 8/6/17.
 */
@Entity
public class MailingAddress {
    String line1;
    String line2;
    String line3;

    String city;
    String state;
    String zip5;
    String zip4;

    @Transient
    String note;

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip5() {
        return zip5;
    }

    public void setZip5(String zip5) {
        this.zip5 = zip5;
    }

    public String getZip4() {
        return zip4;
    }

    public void setZip4(String zip4) {
        this.zip4 = zip4;
    }

    public String getNote() {
        return note + " I'm transient";
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "MailingAddress{" +
                "line1='" + line1 + '\'' +
                ", line2='" + line2 + '\'' +
                ", line3='" + line3 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip5='" + zip5 + '\'' +
                ", zip4='" + zip4 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MailingAddress that = (MailingAddress) o;

        return new EqualsBuilder()
                .append(line1, that.line1)
                .append(line2, that.line2)
                .append(line3, that.line3)
                .append(city, that.city)
                .append(state, that.state)
                .append(zip5, that.zip5)
                .append(zip4, that.zip4)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(line1)
                .append(line2)
                .append(line3)
                .append(city)
                .append(state)
                .append(zip5)
                .append(zip4)
                .toHashCode();
    }
}
